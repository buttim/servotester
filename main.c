#include "N76E003.h"
#include "SFR_Macro.h"
#include "Function_define.h"
#include "Common.h"
#include "Delay.h"

#define MINPOS	600
#define MAXPOS	2400

#define SEGA	P00
#define SEGB	P13
#define SEGC	P30
#define SEGD	P17
#define SEGE	P15
#define	SEGF	P10
#define SEGG	P07

#define DIGIT1	P01
#define DIGIT2	P11
#define DIGIT3	P14
#define DIGIT4	P06

#define SW1	P05
#define SW2 P04

typedef enum { NORMAL, NEUTRAL, SWEEP } Mode;
typedef enum { F50Hz, F125Hz, F250Hz } Freq;

unsigned duty,numberToShow,speed;
Mode mode=NORMAL;
Freq freq=F50Hz;
int direction=1;
int pause=0;

void setFreq(Freq freq) {
	switch (freq) {
		case F50Hz:
			PWMPH=0x9C;	//50Hz
			PWMPL=0x40;
			break;
		case F125Hz:
			PWMPH=0x3E;	//125Hz
			PWMPL=0xB0;
			break;
		case F250Hz:
			PWMPH=0x1F;	//250Hz
			PWMPL=0x40;
			break;
	}
}

void show(int mask) {
	SEGA=(mask&1)!=0;
	SEGB=(mask&2)!=0;
	SEGC=(mask&4)!=0;
	SEGD=(mask&8)!=0;
	SEGE=(mask&16)!=0;
	SEGF=(mask&32)!=0;
	SEGG=(mask&64)!=0;
}

uint8_t getMask(uint8_t n) {
	if (n>11 || n<0) return 0;
	return (unsigned char)"\x3F\x06\x5B\x4F\x66\x6D\x7D\x07\x7F\x6F"[n];
}

void showDigit(uint8_t n) {
	show(getMask(n));
}

void display(int n) {
	static int i=0;
	int j;
	uint8_t c;	

	show(0);
	DIGIT1=i==3&&n>999?0:1;
	DIGIT2=i==2&n>99?0:1;
	DIGIT3=i==1&n>9?0:1;
	DIGIT4=i==0?0:1;

	for (j=0;j<i;j++) n/=10;
	c=n%10;
	showDigit(c);
	if (++i==4) i=0;
}

void ADC_ISR (void) interrupt 11 using 2 {
	unsigned adc;
	
	adc=ADCRH;
  adc<<=4;
	adc=4095-adc;
	
	if (!pause) {
		switch (mode) {
			case NORMAL:
				numberToShow=MINPOS+((unsigned long)(MAXPOS-MINPOS)*adc)/4096;
				duty=(2008UL*MINPOS)/1000+2008UL*(((unsigned long)(MAXPOS-MINPOS)*adc/1000))/4096;
				break;
			case NEUTRAL:
				numberToShow=(MAXPOS+MINPOS)/2;
				duty=2008+2008/2;
				break;
			case SWEEP:
				speed=1+(7UL*adc)/4096;
				break;
		}
		
		PWM0H=duty>>8;
		PWM0L=duty&0xFF;
		set_LOAD;
		set_PWMRUN;
	}
	
	clr_ADCF;                               //clear ADC interrupt flag
	set_ADCS;
}

void WakeUp_Timer_ISR (void) interrupt 17 {
	display(numberToShow);
	clr_WKTF;
}

void Timer0_ISR (void) interrupt 1 using 3 {
	static int count=0;
	if (mode!=SWEEP||pause||++count<10) return;
	count=0;
	numberToShow+=speed*direction;
	if (numberToShow<=MINPOS) {
		numberToShow=MINPOS;
		direction=1;
	}
	else if (numberToShow>=MAXPOS) {
		numberToShow=MAXPOS;
		direction=-1;
	}
	duty=2008+(2008UL*(numberToShow-MINPOS))/(MAXPOS-MINPOS);
}

void initTimers() {
	TMOD=0XFF;
	TIMER0_MODE0_ENABLE;
	TIMER1_MODE0_ENABLE;
	
	set_T0M;
	clr_T1M;
	
	TH0=0xFF;
	TL0=0xFF;
	set_ET0;
	set_TR0;
}

void initPWM() {
	PWM0_P12_OUTPUT_ENABLE;
	PWM_IMDEPENDENT_MODE;
	PWM_CLOCK_DIV_8;
	setFreq(F50Hz);
}

void initWakeupTimer() {
		WKCON=0x0; 										//timer base 10k, Pre-scale = 1/1
		RWK=0XFE;
	  set_EWKT;											// enable WKT interrupt
		set_WKTR; 										// Wake-up timer run 
}

void initADC() {
	Enable_ADC_AIN6;
  set_EADC;
	set_ADCS;
}

void main(void){
	int count1=0,count2=0;
	
	Set_All_GPIO_Quasi_Mode;
	
	P00_PushPull_Mode;	//allows more current for the LED display
	P07_PushPull_Mode;
	P10_PushPull_Mode;
	P13_PushPull_Mode;
	P15_PushPull_Mode;
	P17_PushPull_Mode;
	P30_PushPull_Mode;
	
	initPWM();
	initADC();
	initWakeupTimer();
	initTimers();
  set_EA;
		
	while(1){
		if (SW2==0) count1++;
		else count1=0;
		
		if (count1>50) {
			if (++freq==3) freq=0;
			count1=0;
			setFreq(freq);
			pause=1;
			numberToShow=freq==F50Hz?50:
				freq==F125Hz?125:250;
			//Timer1_Delay10ms(100);
			while(SW2==0);
			pause=0;
		}		

		if (SW1==0) count2++;
		else count2=0;
		
		if (count2>50) {
			if (++mode==3) mode=0;
			while(SW1==0);
			count2=0;
		}		
	}
}

