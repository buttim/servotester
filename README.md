Alternate firmware for Nuvoton N76E300 based servo tester
=======================================

![servo tester](doc/servotester.jpg){width=400 height=400}

Replacement for the original firmware that allows pulse width as low as 600uS and as high as 2400uS instead of the originals 1000uS and 2000uS, giving you a wider range of movent of the attached servos. Some improvements in the user interface too.
More details [on my blog](https://ydiaeresis.wordpress.com/2020/10/10/refurbishing-a-servo-tester/)

This work is licensed under the [Creative Commons CC BY 4.0 License](https://creativecommons.org/licenses/by/4.0/)